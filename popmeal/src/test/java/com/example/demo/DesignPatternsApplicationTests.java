package com.example.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SpringBootTest
class DesignPatternsApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    public void testObjectMapper() throws JsonMappingException, JsonProcessingException {
        String response = "{\"resourceType\":\"OperationOutcome\",\"issues\":[{\"severity\":\"error\",\"code\":\"invalid\",\"details\":{\"coding\":[{\"code\":\"MSG_INVALID_REQUESTFORMAT\"}],\"text\":\"Request does not conform to the expected structure\"}}]}";
        ObjectMapper mapper = new ObjectMapper();
        JsonNode actualObj = mapper.readTree(response);
        JsonNode actualObj1 = actualObj.get("issues").isArray() ? actualObj.get("issues").get(0) : null;
        JsonNode actualObj2 = actualObj1.get("details");
        JsonNode actualObj3 = actualObj2.get("text");
        System.out.println(actualObj3.textValue());

    }

    @Test
    public void test() {
        String s = "myanmar";
        char[] characters = s.toCharArray();
        String duplicatesRemoved = "";
        for (char c : characters) {
            if (!duplicatesRemoved.contains(Character.toString(c))) {
                duplicatesRemoved = duplicatesRemoved + c;
            }
        }
        System.out.println(duplicatesRemoved);
    }

    @Test
    public void test1() {
        String s = "myanmar";
        char[] characters = s.toCharArray();
        String duplicatesRemoved = "";
        Set<Character> set = new HashSet<>();
        for (char c : characters) {

        }
        System.out.println(duplicatesRemoved);
    }

    @Test
    public void test2() {
        String s = "myanmar";
        StringBuilder sb = new StringBuilder();
        int index = 0;
        char[] characters = s.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            char c = s.charAt(i);
            index = s.indexOf(c, i + 1);
            if (index == -1)
                sb.append(c);
        }
        System.out.println(sb);
    }

    @Test
    public void gemstones() {
        // Write your code here
        List<String> rocks = new ArrayList<String>();
        rocks.add("aee");
        rocks.add("aee");
        rocks.add("aee");

        int output = 0;
        int number = 0;
        String firstString = rocks.get(0);
        String uniqueString = "";
        for (char c : firstString.toCharArray()) {
            if (!uniqueString.contains(String.valueOf(c))) {
                uniqueString = uniqueString + String.valueOf(c);
            }
        }
        char[] characters = uniqueString.toCharArray();

        for (char c : characters) {
            for (int i = 1; i < rocks.size(); i++) {
                String eachString = rocks.get(i);

                if (eachString.contains(String.valueOf(c))) {
                    output = 1;
                } else {
                    output = 0;
                    break;
                }
            }
            number = number + output;
        }
        System.out.println(number);
    }

    @Test
    public void testVolleyBall() {
        int a = 3;
        int b = 25;

        BigInteger tw = BigInteger.ONE;
        BigInteger tl = BigInteger.ONE;
        BigInteger num = BigInteger.ONE;
        BigInteger den = BigInteger.ONE;
        BigInteger v24 = BigInteger.ONE;
        BigInteger mo = BigInteger.valueOf(1000000007);
        BigInteger way = BigInteger.ZERO;
        if (a == 25 && b == 24) {
        } else if (a < 25 && b < 25) {
        } else if ((a == 25 && b < 24) || (b == 25 && a < 24)) {

            int c;
            if (a == 25) {
                tw = BigInteger.valueOf(a - 1 + b);
                tl = BigInteger.valueOf(b);
                c = b;
            } else {
                tw = BigInteger.valueOf(b - 1 + a);
                tl = BigInteger.valueOf(a);
                c = a;
            }

            for (int i = 1; i <= c; i++) {
                num = num.multiply(tw);
                tw = tw.subtract(BigInteger.ONE);
                den = den.multiply(tl);
                tl = tl.subtract(BigInteger.ONE);
            }
            way = num.divide(den).mod(mo);

        } else if ((a - b == 2) || (b - a == 2)) {

            v24 = BigInteger.valueOf(801728689);
            num = v24;
            int c;
            if (a > b) {
                c = b - 24;
            } else {
                c = a - 24;
            }
            tw = BigInteger.valueOf(2);
            tw = tw.modPow(BigInteger.valueOf(c + 1), mo);
            way = num.multiply(tw).mod(mo);

        }

        System.out.println(way);
    }

    @Test
    public void testMaxProduct() {
        // int[] nums = { 1, 2, 3, 4, -5 };

        int[] nums = { 1, 2, 3, 0, -1, -15 };
        int product = nums[0];
        for (int i = 0; i < nums.length; i++) {
            int elem = nums[i];
            for (int j = i + 1; j < nums.length; j++) {
                product = product > elem ? product : elem;
                elem = elem * nums[j];
            }
            product = product > elem ? product : elem;
        }

        System.out.println(product);

    }

    @Test
    public void test123() {
        int[] n = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        int pos = 6;

        // int[] n1 = { 7, 8, 9, 4, 5, 6, 1, 2, 3 };

        int[] newArray = new int[n.length];

        List<Integer> numbers = new LinkedList();

        int count = 0;
        for (int i = pos; i < n.length; i++) {
            numbers.add(n[i]);
            count++;
        }
        for (int i = pos - count; i < pos; i++) {
            numbers.add(n[i]);
        }
        for (int i = 0; i < pos - count; i++) {
            numbers.add(n[i]);
        }

        numbers.stream().forEach(m -> System.out.print(m));

    }

    @Test
    public void test1234() {
        int[] even = { 10, 12, 14, 8, 2 };
        int[] odd = { 19, 71, 35, 43 };
    }

    @Test
    public void test99() {

        int[] numbers = { 1, 2, 3, 4, 5, -1, -2, -3, -4, 0 };

        int sum = 7;

        for (int i = 0; i < numbers.length; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                for (int k = j + 1; k < numbers.length; k++) {
                    if (numbers[i] + numbers[j] + numbers[k] == sum) {
                        System.out.println("Numbers are : " + numbers[i] + "," + numbers[j] + "," + numbers[k]);
                    }
                }
            }
        }

    }

    @Test
    public void test101() {

        String s = "abcba";
        char[] character = s.toCharArray();
        Map<Character, Integer> result = new HashMap<>();

        for (Character c : character) {
            if (result.containsKey(c)) {
                result.put(c, result.get(c) + 1);
            } else {
                result.put(c, 1);
            }
        }
        result.entrySet().stream().forEach(
                                        n -> System.out.println(" key Value: " + n.getKey() + " value" + n.getValue()));

    }

    @Test
    public void test102() {
        TestChild obj = new TestChild();
        // obj.testPrint();
    }

    // A obj = new B();
    // obj.m1();



}
