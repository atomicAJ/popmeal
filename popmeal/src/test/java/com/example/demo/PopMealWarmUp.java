package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest
/**
 * @author AkuJain
 * @version 1.0
 * @since Oct 15, 2021
 * 
 */
public class PopMealWarmUp {

    int testCount = 1;

    public void incrementCount() {
        testCount++;
    }

    @Test
    public void camelCasingTest() {
        String str = "thisContainsFourWords";

        // Initalizing with 1 as first word will always start will smaller case
        // TODO
        // Work on the bonus step
        int count = 1;
        for (char c : str.toCharArray()) {
            if (Character.isUpperCase(c))
                count++;
        }

        System.out.println(count);

        // As the code is already two lines only and stream doesnot support charc Array
        // but second solution is in second method
        // not using Stream API here
    }

    @Test
    public void camelCasingTest1() {

        String str = "thisContainsFourWords";
        int count = 1;
        for (int i = 1; i < str.length() - 1; i++) {
            if (str.charAt(i) >= 65 && str.charAt(i) <= 90) {
                count++;
            }
        }
        System.out.println(count);
    }

    @Test
    public void camelCasingTestUsingStream() {

        String str = "thisContainsFourWords";
        int count = 1;
        // Using Stream to check the same
        str.codePoints().mapToObj(c -> (char) c).forEach(c -> {
            if (Character.isUpperCase(c)) {
                incrementCount();
            }
        });
    }

    @Test
    public void testOptimalBreak() {
        int[] tasks = { 1, 4, 1, 3, 1 };
        // int[] tasks = { 3, 4, 5, 6, 7, 8 };
        System.out.println(obtainOptimalBreak(tasks));

    }

    /**
     * @param tasks
     */
    private String obtainOptimalBreak(int[] tasks) {
        for (int i = 1; i < tasks.length; i++) {
            int[] beginningTasks = Arrays.copyOfRange(tasks, 0, i);
            int[] endTasks = Arrays.copyOfRange(tasks, i, tasks.length);
            if (Arrays.stream(beginningTasks).sum() == Arrays.stream(endTasks).sum()) {
                return Integer.toString(i - 1);
            }
        }
        return null;

    }

}
