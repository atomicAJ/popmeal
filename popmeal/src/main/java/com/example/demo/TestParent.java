/**
 * (C) Koninklijke Philips Electronics N.V. 2021
 * 
 * All rights are reserved. Reproduction or transmission in whole or in part, 
 * in  any form or by any means, electronic, mechanical or otherwise, is 
 * prohibited without the prior written permission of the copyright owner.
 * 
 * @author AkuJain
 * 
 * Filename   : TestParent.java
 * 
 * Description : The class TestParent.java.
 * @version  1.0
 */
package com.example.demo;

/**
 * @author AkuJain
 * @version 1.0
 * @since Oct 5, 2021
 * 
 */
public class TestParent {

    /*
     * public void testPrint() { System.out.println("parent invoked"); }
     */

}
