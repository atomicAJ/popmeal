package com.example.demo.insertion;

import java.util.List;

public class InsertionServiceImpl implements InsertionService {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.example.demo.deliveryInsertion.InsertionService#findInsertion(com.example
     * .demo.deliveryInsertion.Delivery, java.util.List)
     */
    public Insertion findInsertion(Delivery deliveryToInsert, List<Route> routes) {

        // Using temporary variable to find minimum time needed for get the between
        // delivery to inserted and existing delivery
        int minTime = -1;
        // Class Variable Insert: as we find the delivery and route with minimum time
        // difference, update the value with least minimum value
        Insertion insert = null;

        // Iterate through each Route and Delivery to be compared with delivery to be
        // inserted
        for (Route route : routes) {
            for (Delivery deliv : route.getDeliveries()) {
                DistanceService disService = new DistanceService() {

                    // Dummy implemenation of getDuration for example only
                    @Override
                    public int getDurationInSeconds(Delivery a, Delivery b) {
                        // imeplemtation get the difference in time
                        return Integer.parseInt(a.getId()) - Integer.parseInt(b.getId());

                    }

                    @Override
                    public int getDistanceIMeters(Delivery a, Delivery b) {
                        return 0;
                    }
                };
                // when minimum value is found populate the Insert object with index,delivery
                // and route to add that to
                int tempMinTime = Math.abs(disService.getDurationInSeconds(deliv, deliveryToInsert));
                // First run always minTime will be -1 and will be overwritten
                // if TempMinTime obtained by interation is lesser than current minTime then
                // only it will be overwritten along with Insert Object
                if (minTime == -1 || minTime > tempMinTime) {
                    minTime = tempMinTime;
                    insert = new Insertion(route, deliv, route.getDeliveries().indexOf(deliv));
                }
            }
        }

        return insert;
    }

    // Bonus Implemenation

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.example.demo.deliveryInsertion.InsertionService#findInsertion(com.example
     * .demo.deliveryInsertion.Delivery, java.util.List)
     */
    public Insertion findInsertionByTimeAndDistance(Delivery deliveryToInsert, List<Route> routes) {

        // Using temporary variable to find minimum time needed for get the between
        // delivery to inserted and existing delivery
        int minTime = -1;
        int minDistance = -1;
        // Class Variable Insert: as we find the delivery and route with minimum time
        // difference, update the value with least minimum value
        Insertion insert = null;

        // Iterate through each Route and Delivery to be compared with delivery to be
        // inserted
        for (Route route : routes) {
            for (Delivery deliv : route.getDeliveries()) {
                DistanceService disService = new DistanceService() {

                    // Dummy implemenation of getDuration for example only
                    @Override
                    public int getDurationInSeconds(Delivery a, Delivery b) {
                        // imeplemtation get the difference in time
                        return Integer.parseInt(a.getId()) - Integer.parseInt(b.getId());

                    }

                    // DUmmy Implemenation for distance
                    @Override
                    public int getDistanceIMeters(Delivery a, Delivery b) {
                        return 0;
                    }
                };

                int tempMinTime = Math.abs(disService.getDurationInSeconds(deliv, deliveryToInsert));
                int tempMinDistance = Math.abs(disService.getDistanceIMeters(deliv, deliveryToInsert));

                // since we have both values distance b/w two deliveries and time between them
                // as well
                // Let us assume priority is optimal difference between time and Distance so if
                // both time and Distance are less then we will overwrite

                if ((minTime == -1 || minTime > tempMinTime) && (minDistance == -1 || minDistance > tempMinDistance)) {
                    minTime = tempMinTime;
                    minDistance = tempMinDistance;
                    insert = new Insertion(route, deliv, route.getDeliveries().indexOf(deliv));
                }
            }
        }

        return insert;
    }

}
