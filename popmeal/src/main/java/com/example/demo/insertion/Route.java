package com.example.demo.insertion;

import java.util.List;

public interface Route {
    List<Delivery> getDeliveries();
}