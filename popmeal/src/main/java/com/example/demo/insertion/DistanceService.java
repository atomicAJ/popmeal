package com.example.demo.insertion;

public interface DistanceService {

    /**
     * Returns the duration in seconds to get from Delivery `a` to Delivery `b`. The
     * following always holds: getDistance(a, b) == getDistance(b, a)
     */
    int getDurationInSeconds(Delivery a, Delivery b);

    int getDistanceIMeters(Delivery a, Delivery b);
}