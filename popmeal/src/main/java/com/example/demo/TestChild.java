/**
 * (C) Koninklijke Philips Electronics N.V. 2021
 * 
 * All rights are reserved. Reproduction or transmission in whole or in part, 
 * in  any form or by any means, electronic, mechanical or otherwise, is 
 * prohibited without the prior written permission of the copyright owner.
 * 
 * @author AkuJain
 * 
 * Filename   : TestChild.java
 * 
 * Description : The class TestChild.java.
 * @version  1.0
 */
package com.example.demo;

/**
 * @author AkuJain
 * @version 1.0
 * @since Oct 5, 2021
 * 
 */
public class TestChild extends TestParent {

    /*
     * @Override public void testPrint() { System.out.println("child invoked"); }
     */

}
